/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;
import modelo.Grupo;

/**
 *
 * @author estela
 */
public class VistaGrupo implements IVista <Grupo>{
 

    public Grupo cogerDatos() {
        Grupo grupo = new Grupo();
        System.out.println("Nombre : ");
        Scanner sc = new Scanner(System.in);
        String nombre = sc.nextLine();
        System.out.println("Id: ");
        int id = sc.nextInt();
        grupo.setNombre(nombre);
        grupo.setid(id);
        return grupo;
    }
    
    public void mostrarDatos(Grupo grupo){
        System.out.println("MOSTRAR DATOS");
        System.out.println("NOMBRE: " + grupo.getNombre());
        System.out.println("ID: " + grupo.getid());
    }  
}
