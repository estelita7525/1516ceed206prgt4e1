/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

/**
 *
 * @author estela
 */
public interface IVista<T> {// La T es un tipo de objeto. Podemos llamarle como queramos
        public T cogerDatos();
        public void mostrarDatos (T t);
            
    
}
