/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;
import modelo.Alumno;

/**
 *
 * @author estela
 */
public class VistaAlumno implements IVista<Alumno> {
    public Alumno cogerDatos() {
        Alumno alumno = new Alumno();
        System.out.println("Nombre : ");
        Scanner sc = new Scanner(System.in);
        String nombre = sc.nextLine();
        System.out.println("Id: ");
        int id = sc.nextInt();
        alumno.setNombre(nombre);
        alumno.setid(id);
        return alumno;
    }
    
    public void mostrarDatos(Alumno alumno){
        System.out.println("MOSTRAR DATOS");
        System.out.println("NOMBRE: " + alumno.getNombre());
        System.out.println("ID: " + alumno.getid() + "\n");
        VistaGrupo vg = new VistaGrupo();
        vg.mostrarDatos(alumno.getGrupo());
    }

}
