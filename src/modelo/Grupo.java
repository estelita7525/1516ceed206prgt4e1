/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author estela
 */
public class Grupo {
    private String nombre;
  private int id;  

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the id
     */
    public int getid() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setid(int id) {
        this.id = id;
    }
}
