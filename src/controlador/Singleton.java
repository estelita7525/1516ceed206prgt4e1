/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author estela
 */
public class Singleton {
private static Singleton instancia;
private String nombre;
    private Singleton (){//esto convierte el constructor en privado para controlar la instancia
    
    
} 
    private Singleton (String n){
        nombre=n;
   
    }
    public static Singleton getInstancia(String n){
        if (instancia==null){
            instancia=new Singleton(n);
            System.out.println("Instancia creada correctamente");
        } else {
            System.err.println("ya existe instancia de esta clase");
        }
        return instancia;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
