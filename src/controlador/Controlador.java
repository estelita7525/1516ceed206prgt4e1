/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Alumno;
import modelo.Grupo;
import vista.IVista;
import vista.Vista;
import vista.VistaAlumno;
import vista.VistaGrupo;

/**
 *
 * @author estela
 */
public class Controlador {
private Alumno alumno;
private Vista vista;
private Grupo grupo=new Grupo();
private IVista<Grupo> vg =new VistaGrupo();
private IVista<Alumno> va=new VistaAlumno();

public Controlador() {
//Constructor por defecto
}    

public Controlador (Alumno a,Vista v){
    alumno=a;//igualar las variable - parametros. a la izquierda el nombre de lo que tiene que tomar el valor 
    vista=v; //se trabaja de izquierda a derecha
    int opcion;
    boolean bucle= true;
    
    Singleton.getInstancia("nombre app.");
     Singleton.getInstancia("app.");
    
    do {
        opcion= vista.menuPrincipal();
    switch(opcion){
        case 0:
            return;
         case 1: 
             grupo=vg.cogerDatos();
            break;
         case 2:
             alumno=va.cogerDatos();
             alumno.setGrupo(grupo);
             bucle=false;
            break;
    }
    } while (bucle);

    va.mostrarDatos(alumno);
       
}

}
